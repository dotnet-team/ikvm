ikvm (8.1.5717.0+ds-1) unstable; urgency=medium

  * [a948bb2] Update so get-orig-source gets the right version
  * [3ca6eb0] Imported Upstream version 8.1.5717.0+ds
  * [50bdfab] Refreshed debian/patches/01-use_system_SharpZipLib.patch
  * [8915f6f] Refreshed debian/patches/03-use_mono.snk_for_ikvm-key.patch
  * [8a757b6] Removed d/p/force_4.0_Mono.CompilerServices.SymbolWriter
  * [84a8679] Fix build-deps (Closes: #814164)
  * [6925689] Fix install locations
  * [b4a395f] Fix upstream Linux build bugs
  * [8fcea15] Patch implib.exe to support SNK from files, not Crypto Store
  * [297fbed] Add new assemblies to GAC and pkg-config
  * [49c4e64] Override JSON license lintian check. The quoted license doesn't
    apply to any files in the tree.

 -- Jo Shields <jo.shields@xamarin.com>  Fri, 19 Feb 2016 14:40:20 +0000

ikvm (7.2.4630.5+ds-2) unstable; urgency=medium

  * [73b5da0] Force inclusion of 4.0 version of Mono.CompilerServices.SymbolWriter

 -- Jo Shields <jo.shields@xamarin.com>  Wed, 11 Nov 2015 13:14:08 +0000

ikvm (7.2.4630.5+ds-1) unstable; urgency=low

  * [4ed30ed] Imported Upstream version 7.2.4630.5+ds
  * [985e638] Bump DEB_CLI_API_VERSION
  * [0cf77da] Bump JAVATARBALL
  * [25ec57f] Refreshed debian/patches/01-use_system_SharpZipLib.patch
  * [456126d] Refreshed debian/patches/03-use_mono.snk_for_ikvm-key.patch
  * [175a2ec] Remove debian/patches/unicode_on_windows_is_crap.patch, 
    obsolete upstream.
  * [c3e444b] Refresh install files.

 -- Jo Shields <directhex@apebox.org>  Fri, 11 Oct 2013 15:51:47 +0200

ikvm (7.0.4335.0+ds-1) unstable; urgency=low

  * [753c2d1] Tweak get-orig-source rule to handle new taball locations
  * [d62de02] Imported Upstream version 7.0.4335.0+ds
  * [4a55d7a] Refreshed debian/patches/01-use_system_SharpZipLib.patch
  * [8b01a7d] Refreshed debian/patches/03-use_mono.snk_for_ikvm-key.patch
  * [bb28a50] Refresh path for native library in debian/libikvm-native.install
  * [90e98db] Build-depend on OpenJDK 7, not 6, for this IKVM release.
  * [b68c6b3] Build fails due to stray non-Unicode character in one file. 
    Remove it.
  * [300b38f] Don't delete charsets.jar or localedata.jar in clean, we need 
    these ones
  * [a5a662e] Re-add uscan rule which was disabled for debugging
  * [02223cf] Fix location of IKVM in debian/ikvm.install
  * [983874e] Add new Windows-only modulerefs to ignore
  * [ad13aac] Bump DEB_CLI_API_VERSION

 -- Jo Shields <directhex@apebox.org>  Tue, 07 Feb 2012 18:28:05 +0000

ikvm (0.46.0.1+ds-5) unstable; urgency=low

  [ Iain Lane ]
  * [cc291a9] No need to uu{en,de}code with 3.0 (quilt); drop this
  * [5447e44] Fix Vcs-Browser URL

  [ Jo Shields ]
  * [b7ec5c7] Clean up rules, and use --with rather than deprecated cli.make

 -- Jo Shields <directhex@apebox.org>  Fri, 20 Jan 2012 14:27:21 +0000

ikvm (0.46.0.1+ds-4) unstable; urgency=low

  * [bd6e60c] Reintroduce signing of builds, for GACability.
  * [e694acb] Install IKVM libraries to the GAC.

 -- Jo Shields <directhex@apebox.org>  Sat, 06 Aug 2011 13:13:20 +0100

ikvm (0.46.0.1+ds-3) unstable; urgency=low

  * [9a1252d] Move glib build-dep as well, since it's used for the .so.
  * [c1edce3] Fix Arch/Indep split again. For real, this time. No more FTBFS.

 -- Jo Shields <directhex@apebox.org>  Wed, 06 Jul 2011 02:01:07 +0100

ikvm (0.46.0.1+ds-2) unstable; urgency=low

  * [85e6685] Move cli-common-dev to Build-Depends, not B-D-I. Prevents FTBFS.

 -- Jo Shields <directhex@apebox.org>  Wed, 06 Jul 2011 00:49:33 +0100

ikvm (0.46.0.1+ds-1) unstable; urgency=low

  * [3790281] initial upstream branch
  * [275caa3] Add explicit build-depends on libglib-dev.
  * [491d57f] Imported Upstream version 0.46.0.1+ds
  * [23e23b1] Update build-dependencies for the last transition. Whoops.
  * [47010df] Refresh Vcs-* fields.
  * [2da3694] Bump Debian Standards version to 3.9.2.
  * [48f2886] Update debian/watch to use Debian redirector.
  * [8520086] Refresh get-orig-source rule for crap-free upstream tarball.
  * [4521f1a] Move from standalone Quilt to DebSrc 3.0 (quilt).
  * [fca7bbe] Include specialist cli-nant.make from cli-common-dev.
  * [266fca1] Bump ABI and API versions, for packaging metadata.
  * [9f10b32] Bump debhelper build-dep, for override_* powers.
  * [f5cc8e6] Refresh ikvm pkgconfig file for latest libs.
  * [f45584d] Massively rewrite rules file, using DH7 semantics.
  * [77bc433] Delete 04-decouple_arch_build_from_indep.patch (now obsolete).
  * [71bd418] Delete 05-produce-rmi-at-compile-time.patch (now obsolete).
  * [b10be78] Refreshed 01-use_system_SharpZipLib.patch.
  * [ba573ac] Add a master DH7 rule.
  * [e9a25c0] Don't blow away ALL .jar files - some have needed bare 
    manifests.
  * [c853561] Add empty dh_auto_build_nant rule, as ikvm.build is not in 
    $CURDIR.
  * [2ce05f8] Include both cli.make and cli-nant.make. They're not exclusive.
  * [2d3a12a] Exclude Fusion moduleref - this lib is only used on MS.NET on 
    Win32.
  * [83e244f] Exclude moduleref for winspool.drv. Obviously not for us.
  * [f727f77] Hardcode some assembly locations, as upstream expects libs & 
    compiler in the same folder.
  * [868cddd] Remove traces of IKVM being in the GAC. It's not a GAC lib 
    package, and the GAC was only ever a convenience with side-effects.
  * [3adc876] Don't let the native .so file go into the IKVM package.

 -- Jo Shields <directhex@apebox.org>  Tue, 05 Jul 2011 23:55:04 +0100

ikvm (0.40.0.1+dfsg-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    + Fix Vcs-* fields
    + Bump to standards version 3.8.1
  * debian/IKVM.OpenJDK.ClassLibrary.dll.config:
    + Renamed to IKVM.OpenJDK.Core.dll.config
  * debian/ikvm.pc.in:
    + Pull in the whole family of OpenJDK references, since the ClassLibrary
      assembly has been split into pieces, but those pieces all interdepend
  * debian/rules:
    + Ensure CLI policy folder is actually used for get-orig-source
    + Stop deleting JniInterface.cs
    + Build refemit before ikvmc
    + Add new libraries to messy bootstrapping procedure
    + Update API/ABI version
  * debian/ikvm.installcligac:
    + Update list of assemblies
  * debian/patches/01-use_system_SharpZipLib.patch,
    debian/patches/03-use_mono.snk_for_ikvm-key.patch,
    debian/patches/04-decouple_arch_build_from_indep.patch:
    + Update for new release
  * debian/patches/02-use_ikvm-key_for_openJDKConfiguration.patch,
    debian/patches/00-use_ikvm-key_for_JniInterface.patch:
    + Removed, fixed upstream
  * debian/patches/05-produce-rmi-at-compile-time.patch:
    + Patch from upstream to drop the .class files bundled in their
      OpenJDK source release - huge thanks to Jeroen Frijters for sorting
      this out

 -- Jo Shields <directhex@apebox.org>  Thu, 11 Jun 2009 09:17:15 +0100

ikvm (0.38.0.2+dfsg-2) unstable; urgency=low

  [ Mirco Bauer ]
  * Upload to unstable.

  [ Iain Lane ]
  * Parse the version number from the changelog instead of using uscan -
    buildds do not have internet access.
  * Call dh_prep instead of dh_clean -k (lintian) 

 -- Iain Lane <laney@ubuntu.com>  Mon, 12 Jan 2009 19:49:40 +0000

ikvm (0.38.0.2+dfsg-1) experimental; urgency=low

  [ Jo Shields ]
  * Package adopted by Debian CLI Libraries Team. (Closes: #466336)
  * New upstream release, based on GNU Classpath 0.95 and OpenJDK 6b12
  * Repackaged - uses completely generated .orig, see README.source
  * Updated build-deps for the Mono 2.0 transition.
  * Group Policy:
    + get-orig-source target in debian/rules
  * Signed using Mono key - it causes an ABI breakage for now, but it's an
    improvement on the randomly generated keys used previously, as it means
    ABI breakage is no longer guaranteed between compiles. (Closes: #484125)
  * Use correct path in ikvm.pc file. (Closes: #504529)
  * Ensure that Configuration.java uses the ikvm-key configured for the build,
    not a hard-coded value. (Closes: #337414)

  [ David Paleino ]
  * debian/compat bumped to 7

  [ Mirco Bauer ]
  * debian/control:
    + Added Homepage, Vcs-Svn and Vcs-Browser fields.
    + Enhanced short and long package descriptions.
    + Changed Section from devel to interpreters, as this is mainly a runtime
      and not a development tool.
    + Bumped cli-common-dev build-dep to >= 0.5.7 as we need dh_clistrip and
      dh_clixfixperms of it.
  * debian/rules:
   + Fixed order of dh_* calls.
   + Added dh_clistrip and dh_clifixperms calls.
   + Removed unneeded chmod calls (as dh_clifixperms takes now care).
   + Replaced hardcoded mono calls with DEB_CLI_RUNTIME variable.
  * debian/rules
    debian/IKVM.Runtime.dll.config
    debian/IKVM.Runtime.JNI.dll.config
    debian/IKVM.OpenJDK.ClassLibrary.dll.config:
   + Added missing dll-maps for libikvm-native.so invocation.

 -- Jo Shields <directhex@apebox.org>  Tue,  6 Jan 2009 16:24:02 +0000

ikvm (0.34.0.4-4) unstable; urgency=high

   * QA Upload
   * Build-Depend on libmono-winforms2.0-cil to fix FTBFS (Closes: #458676)
   * [Lintian] debian/control, debian/rules: remove dpatch support
   * [Lintian] Remove .UR and .UE from debian/*.1, and escape hypen (ikvmc.1:59)

 -- Albin Tonnerre <albin.tonnerre@gmail.com>  Mon, 02 Jun 2008 22:10:05 +0200

ikvm (0.34.0.4-3) unstable; urgency=low

   * Orphan ikvm: set maintainer to QA Group

 -- Dave Beckett <dajobe@debian.org>  Sun, 17 Feb 2008 21:20:31 -0800

ikvm (0.34.0.4-2) unstable; urgency=low

   * debian/control: ikvm is now only for arches amd64, i386 and powerpc
     and consequently failure to build on IA64 or S390 is no longer relevant
     (Closes: #382156, #332516)
   * debian/control: altered to use source:version (Closes: #435975)
   * debian/control: added Homepage

 -- Dave Beckett <dajobe@debian.org>  Sun, 9 Dec 2007 13:37:00 -0800

ikvm (0.34.0.4-1) unstable; urgency=low

   * New upstream release
   * Replace ecj-bootstrap-gcj with ecj-gcj (Closes: #441512)

 -- Dave Beckett <dajobe@debian.org>  Sat, 15 Sep 2007 19:21:16 -0700

ikvm (0.34.0.2-1) unstable; urgency=low

   * New upstream release (Closes: #422898)
   * Build using GNU Classpath 0.95 sources.
   * Removed patch 01-MemberWrapper.cs.patch- fix merged upstream
   * Invoke dh_clideps with -l args to prevent monodis core dumping
     (Closes: #393691)
   * Update build-dependencies (Closes: #415644)
   * Require nant 0.85-2 hoping that 382156 (never diagnosable)
     might be fixed in the release 0.85 compared to 0.85-rc4 it was
     reported on.
   * Added patch
     classpath-patches/01-ejc-InflaterDynHeader-continue-bad-lineno.patch
     to classpath 0.95 since despite what the Eclipse "FIXED" bug
     https://bugs.eclipse.org/bugs/show_bug.cgi?id=119175 says, ecj 3.3.0
     still fails to compile java/util/zip/InflaterDynHeader.java correctly
     when a continue is optimised away.
   * Made this junk work again, reluctantly remain maintainer (Closes: #423244)

 -- Dave Beckett <dajobe@debian.org>  Sun, 24 Jun 2007 19:52:47 -0700

ikvm (0.30.0.0-1) unstable; urgency=low

   * New upstream release
   * Build using GNU Classpath 0.92 sources.
   * Added patch 01-MemberWrapper.cs.patch
   * Build-Depends: on cli-common-dev >= 0.4.4

 -- Dave Beckett <dajobe@debian.org>  Thu, 19 Oct 2006 00:35:37 -0700

ikvm (0.28.0.0-1) unstable; urgency=medium

   * New upstream release
   * Build using GNU Classpath 0.91 sources.
   * Debhelper 5
   * Standards-Version 3.7.2
   * Require mono-gmcs to get gmcs.exe (Closes: #380133)

 -- Dave Beckett <dajobe@debian.org>  Thu, 27 Jul 2006 21:28:00 -0700

ikvm (0.26.0.1-2) unstable; urgency=low

  * Build with ecj (actually the GCJ-compiled ecj in package
    ecj-bootstrap-gcj) rather than ecj-bootstrap (Closes: #364430)

 -- Dave Beckett <dajobe@debian.org>  Tue, 25 Apr 2006 22:39:04 -0700

ikvm (0.26.0.1-1) unstable; urgency=low

  * New upstream release
  * Build using GNU Classpath 0.90 sources.

 -- Dave Beckett <dajobe@debian.org>  Thu, 23 Mar 2006 21:13:38 -0800

ikvm (0.24.0.1-1) unstable; urgency=low

  * New upstream release
  * Build using GNU Classpath 0.20 sources.
  * Builds on amd64 and more importantly now works due to something
    changing in one of the above. (Closes: #327407) 

 -- Dave Beckett <dajobe@debian.org>  Mon, 30 Jan 2006 18:42:47 -0800

ikvm (0.22.0.0-1) unstable; urgency=low

  * New upstream release
  * Build using GNU Classpath 0.19 sources.
  * Build-Depend on mono 1.1.9+ as mono 1.1.8 runtime dies on the output
    with invalid IL code.

 -- Dave Beckett <dajobe@debian.org>  Sat, 14 Jan 2006 19:12:46 -0800

ikvm (0.18.0.0-2) unstable; urgency=low

  * Use XDG_CONFIG_HOME to make signing write in build area not buildd
    user home (Closes: #320710)
  * Depend on mono-classlib-2.0 to get both 1.0 and 2.0 profiles
    available to ikvm (but not yet built).
  * Allow use of eclipse-ecj to build with if present.
  * Depend on java-gcj-compat to get a JVM environment for ecj-bootstrap

 -- Dave Beckett <dajobe@debian.org>  Thu,  8 Sep 2005 11:54:48 +0100

ikvm (0.18.0.0-1) unstable; urgency=low

  * New upstream release
  * Build using GNU Classpath 0.17 sources
  * debian/control, debian/rules: Use ${Source-Version} in control, no
    need for ikvm:debversion substvar
  * Add Depends: for libikvm-native
  * Do not install executable .dll files
  * Update debian/watch again to use http://qa.debian.org/watch/sf.php to
    try to follow sourceforge updates.
  * Use ecj-bootstrap 3.0.x to compile GNU Classpath 0.17 as both gcj 4.0
    and eclipse-java do not support java 1.5

 -- Dave Beckett <dajobe@debian.org>  Sun, 31 Jul 2005 19:06:18 +0100

ikvm (0.16.0.0-1) unstable; urgency=low

  * New upstream release
  * Build using GNU Classpath 0.16.0.0 sources
  * Require mono 1.1.8.2
  * Remove 01-JniInterface-delfix.patch applied upstream
  * Build strong-named DLLs
  * Install DLLs into the GAC
  * Do not install ICSharpCode.SharpZipLib.dll as it is shipped with 
    the main mono distribution

 -- Dave Beckett <dajobe@debian.org>  Tue, 12 Jul 2005 21:52:57 +0100

ikvm (0.14.0.1-8) unstable; urgency=low

  * Added new manual pages for ikvm, ikvmc and ikvmstub

 -- Dave Beckett <dajobe@debian.org>  Sun, 19 Jun 2005 23:29:17 +0100

ikvm (0.14.0.1-7) unstable; urgency=low

  * Restore older piped uudecode method to use less disk space
  * Install a pkgconfig file ikvm.pc built from debian/ikvm.pc.in to
    allow mono 1.1.8 packages build against this ikvm.

 -- Dave Beckett <dajobe@debian.org>  Sun, 19 Jun 2005 13:18:03 +0100

ikvm (0.14.0.1-6) unstable; urgency=low

  * Fixed debian/watch file

 -- Dave Beckett <dajobe@debian.org>  Sat, 18 Jun 2005 14:51:33 +0100

ikvm (0.14.0.1-5) unstable; urgency=low

  * Removed obsolete Build-Depend: on mono-utils
  * Simplify uudecode of included classpath tarball in attempt to get
    it working inside a buildd environment

 -- Dave Beckett <dajobe@debian.org>  Sat, 18 Jun 2005 11:11:27 +0100

ikvm (0.14.0.1-4) unstable; urgency=low

  * Upload to unstable now that nant and mono 1.1.x are in unstable
  * Added debian/watch file

 -- Dave Beckett <dajobe@debian.org>  Thu, 16 Jun 2005 12:36:42 +0100

ikvm (0.14.0.1-3) experimental; urgency=low

  * Make ikvm depend on exact same version of libikvm-native
  * Make binary-arch and binary-indep build binaries properly (Closes: 311182)

 -- Dave Beckett <dajobe@debian.org>  Sun, 29 May 2005 23:06:58 +0100

ikvm (0.14.0.1-2) experimental; urgency=low

  * Switch to Build-Depends rather than Build-Depends-Indep (Closes: #310300)
  * Use dpatch to apply patch 01-JniInterface-delfix.patch
  * Added libmono-dev and dpatch to Build-Depends
  * Confirmed package builds under pbuilder

 -- Dave Beckett <dajobe@debian.org>  Mon, 23 May 2005 09:24:43 +0100

ikvm (0.14.0.1-1) experimental; urgency=low

  * New upstream release (Closes: #299058)
  * Updated to include GNU classpath 0.15 sources
  * Uses new mono 1.1.x experimental debs (Closes: #307100)
  * Updated to new Mono packaging conventions
  * Added libikvm-native package for native part of IKVM core

 -- Dave Beckett <dajobe@debian.org>  Sat, 21 May 2005 18:50:45 +0100

ikvm (0.12.0.0-1) unstable; urgency=low

  * New upstream release
  * Updated to include GNU classpath 0.14 sources

 -- Dave Beckett <dajobe@debian.org>  Mon, 16 May 2005 23:27:08 +0100

ikvm (0.8.0.0-2) unstable; urgency=low

  * New Maintainer (closes: 305459)

 -- Dave Beckett <dajobe@debian.org>  Thu, 21 Apr 2005 22:43:11 +0100

ikvm (0.8.0.0-1) unstable; urgency=low

  * Initial Release.  Closes: #268233.

 -- John Goerzen <jgoerzen@complete.org>  Thu, 26 Aug 2004 10:18:19 -0500

